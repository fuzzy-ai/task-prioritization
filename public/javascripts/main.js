$(document).ready(function() {
  if ($('#tasks-container').length > 0) {
    $.get("/api/tasks", function(tasks) {
      var html = tasks.map(function(t) {
        return $(task({task: t}));
      });
      $('#tasks-container').children().remove();
      $('#tasks-container').append(html);
    });
  }
});
