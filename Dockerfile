FROM mhart/alpine-node:8

EXPOSE 80
EXPOSE 443

HEALTHCHECK CMD /usr/bin/wget --spider http://localhost:${PORT}/api/ready

ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]
CMD ["npm", "start"]

RUN apk add --no-cache ca-certificates && update-ca-certificates && apk add --no-cache openssl
RUN wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.1.3/dumb-init_1.1.3_amd64
RUN chmod +x /usr/local/bin/dumb-init

WORKDIR /opt/app
ADD . .

RUN npm install
