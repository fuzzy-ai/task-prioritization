// models/user.js

db = require('databank');
var uuid = require('uuid/v4');

User = db.DatabankObject.subClass('user');

User.schema = {
  pkey: "id",
  fields: [
    "accessToken",
    "syncToken",
    "todoistID",
    "createdAt",
    "updatedAt"
  ]
};

User.beforeCreate = function (props, callback) {
  if (!props.id) {
    console.log("generating user id");
    props.id = uuid();
  }

  callback(null, props);
}

module.exports = User;
