var express = require('express');
var router = express.Router();

var _ = require('lodash');
var request = require('request');
var moment = require('moment');

var FuzzyAIClient = require('fuzzy.ai');
var client = new FuzzyAIClient({key: process.env.FUZZYAI_KEY});

router.get('/live', function(req, res, next) {
  res.json({"status": "OK", "message": "Server is live"});
});

router.get('/ready', function(req, res, next) {
  res.json({"status": "OK", "message": "Server is live"});
});

router.get('/tasks', function(req, res, next) {
  // sync Todoist tasks
  var sync_token = (_.isEmpty(req.user.tasks)) ? '*' : req.user.syncToken;
  var params = {
    form: {
      token: req.user.accessToken,
      sync_token: sync_token,
      resource_types: '["all"]'
    }
  }
  request.post('https://todoist.com/api/v7/sync', params, function(err, response, body) {
    var json = JSON.parse(body);

    req.user.syncToken = json.sync_token;
    req.user.tasks = _.unionBy(json.items, req.user.tasks, 'id');
    var inputs = req.user.tasks.map(function(task) {
      var due = moment(task.due_date_utc);
      var created = moment(task.date_added);
      var input = {priority: task.priority};

      var until_due = Math.floor(moment.duration(due.diff(moment())).asDays());
      if (!_.isNaN(until_due)) {
        input['time until due date'] = until_due;
      }

      var since_created = Math.floor(moment.duration(moment().diff(created)).asDays());
      if (!_.isNaN(since_created)) {
        input['time since creation'] = since_created;
      }
      return input;
    });

    client.evaluate(process.env.FUZZYAI_AGENTID, inputs, true, function(err, outputs) {
      if (err) {
        console.error(err);
        next(err);
      } else {
        var results = req.user.tasks.map(function(task, i) {
          return Object.assign({}, task, {
            "urgency": outputs[i].urgency
          });
        });
        results.sort(function(a, b) {
          if (a.urgency < b.urgency) {
            return 1;
          } else if (a.urgency > b.urgency) {
            return -1;
          } else {
            return 0;
          }
        });

        req.user.tasks = results;
        req.user.save(function(err, user) {
          if (err) {
            console.error(err);
            res.status(500).end();
          } else {
            res.json(user.tasks);
          }
        });
      }
    });
  });
});

module.exports = router;
