var express = require('express');
var router = express.Router();
var passport = require('passport')
var TodoistStrategy = require('passport-todoist').Strategy;

var User = require('../models/user');

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.get(id, function(err, user) {
    done(err, user);
  });
});

passport.use('todoist', new TodoistStrategy({
    clientID: process.env.TODOIST_KEY,
    clientSecret: process.env.TODOIST_SECRET
},
  function(accessToken, _, todoist, done) {
    User.search({todistID: todoist.id}, function(err, users) {
      if (err) {
        console.error(err);
        return done(err);
      } else {
        if (users.length == 0) {
          // create new user
          User.create({accessToken: accessToken, syncToken: todoist.sync_token, todistID: todoist.id}, function(err, user) {
            if (err) {
              return done(err);
            } else {
              return done(null, user);
            }
          });
        } else {
          return done(null, users[0]);
        }
      }
    });
  }
));

router.get('/todoist',
  passport.authenticate('todoist', { scope: 'data:read_write', session: true}));

router.get('/todoist/callback',
  passport.authenticate('todoist', {successRedirect: '/', failureRedirect: '/', session: true}));

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

module.exports = router;
