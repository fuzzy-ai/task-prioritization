var path = require('path');
var fs = require('fs');

var express = require('express');
var router = express.Router();
var pug = require('pug');

var cache = {};

var getTemplate = function(name, callback) {
  if (cache[name]) {
    callback(null, cache[name]);
  } else {
    fs.readFile(path.join(__dirname, '..', 'views', `${name}.pug`), 'utf8', function(err, src) {
      if (err) {
        callback(err);
      } else {
        cache[name] = pug.compileClient(src, {"name": name});
        callback(null, cache[name]);
      }
    });
  }
};

/* GET home page. */
router.get('/task', function(req, res, next) {
  getTemplate('task', function(err, code) {
    res.set('Content-Type', 'application/javascript');
    res.send(code);
    res.end();
  });
});

module.exports = router;
