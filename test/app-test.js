// app-test.js
// Copyright 2017 Fuzzy.ai
// All rights reserved

const {spawn} = require('child_process');
const path = require('path');

const vows = require('perjury');
const assert = vows.assert;
const waitForPort = require('wait-for-port');
const Browser = require('zombie');
const debug = require('debug')('task-prioritization:app-test');

let env = Object.assign({}, process.env, {
  ADDRESS: "localhost",
  PORT: "8080",
  KEY: "kohcuk6crjva",
  ID: "ZO2JCF3VGXQV7DGUFEAYCSIF6Y"
});

Browser.localhost(env.ADDRESS, env.PORT);

checkURL = function(url) {
  return {
    topic: function() {
      let br = new Browser();
      let cb = this.callback;
      br.fetch(`http://${env.ADDRESS}:${env.PORT}${url}`)
        .then(function(response) {
          if (response.status == 200) {
            cb(null);
          } else {
            cb(new Error(`Unexpected status code: ${response.status}`));
          }
        })
        .catch(function(err) {
          cb(err);
        });
      return undefined;
    },
    "it works": function(err) {
      assert.ifError(err);
    }
  };
};

vows.describe('App start/stop')
  .addBatch({
    "When we start the app": {
      "topic": function() {
        let www = spawn(path.join(__dirname, "..", "bin", "www"), [], {env: env, silent: true});
        www.stdout.on('data', (data) => {
          debug(`stdout: ${data}`);
        });
        www.stderr.on('data', (data) => {
          debug(`stderr: ${data}`);
        });
        this.callback(null, www);
        return undefined;
      },
      "it works": function(err, www) {
        assert.ifError(err);
        assert.isObject(www);
      },
      "teardown": function(www) {
        if (www) {
          www.kill();
        }
      },
      "and we wait for it to be listening": {
        topic: function() {
          waitForPort(env.ADDRESS, env.PORT, this.callback);
          return undefined;
        },
        "it works": function(err) {
          assert.ifError(err);
        },
        "and we request the index page": {
          topic: function() {
            let br = new Browser();
            let cb = this.callback;
            br.visit(`http://${env.ADDRESS}:${env.PORT}/`, function() {
              debug(br);
              if (br.errors && br.errors.length > 0) {
                cb(br.errors[0], null);
              } else {
                cb(null, br);
              }
            });
          },
          "it works": function(err, br) {
            assert.ifError(err);
            if (br) {
              br.assert.success();
            }
          },
          "it has JQuery": function(err, br) {
            assert.ifError(err);
            if (br) {
              br.assert.element("script[src='/jquery/jquery.min.js']");
            }
          },
          "it has Bootstrap": function(err, br) {
            assert.ifError(err);
            if (br) {
              br.assert.element("script[src='/bootstrap/js/bootstrap.min.js']");
              br.assert.element("link[href='/bootstrap/css/bootstrap.min.css']");
            }
          },
          "it has the local CSS file": function(err, br) {
            assert.ifError(err);
            if (br) {
              br.assert.element("link[href='/stylesheets/style.css']");
            }
          },
          "the home menu item is active": function(err, br) {
            assert.ifError(err);
            if (br) {
              br.assert.element("li.active>a[href='/']")
            }
          }
        },
        "and we request the JQuery source file": checkURL('/jquery/jquery.min.js'),
        "and we request the Bootstrap source file": checkURL('/bootstrap/js/bootstrap.min.js'),
        "and we request the Bootstrap CSS file": checkURL('/bootstrap/css/bootstrap.min.css'),
        "and we request the Cover CSS file": checkURL('/stylesheets/cover.css'),
        "and we request the local CSS file": checkURL('/stylesheets/style.css')
      }
    }
  })
  .export(module);
