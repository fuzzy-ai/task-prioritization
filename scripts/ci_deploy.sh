#!/bin/bash

# install gcloud components
if [ ! -d ${HOME}/google-cloud-sdk ]; then
  curl https://sdk.cloud.google.com | bash;
fi

gcloud components install kubectl -q

# configure gcloud for travis service account
gcloud config set container/use_client_certificate True
gcloud auth activate-service-account --key-file=client-secret.json
gcloud config set compute/zone us-west1-b
gcloud config set project $GCLOUD_PROJECT_ID
gcloud config set account $GCLOUD_ACCOUNT

if [ ! -z $TRAVIS_TAG ]; then
  gcloud container clusters get-credentials $GCLOUD_CLUSTER
  docker tag fuzzyai/$PROJECT gcr.io/$GCLOUD_PROJECT_ID/$PROJECT:$TRAVIS_TAG
  gcloud docker -- push gcr.io/$GCLOUD_PROJECT_ID/$PROJECT:$TRAVIS_TAG
  docker tag fuzzyai/$PROJECT gcr.io/$GCLOUD_PROJECT_ID/$PROJECT:latest
  gcloud docker -- push gcr.io/$GCLOUD_PROJECT_ID/$PROJECT:latest
  # Deploy the new tag
  kubectl set image deployment/$PROJECT $PROJECT=gcr.io/$GCLOUD_PROJECT_ID/$PROJECT:$TRAVIS_TAG
fi
